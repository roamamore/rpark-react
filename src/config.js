export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyDIDhxuXpdvWL8sOqb8AIsNfHVqxqBKdVQ",
  authDomain: "todotest-836ed.firebaseapp.com",
  databaseURL: "https://todotest-836ed.firebaseio.com",
  storageBucket: "todotest-836ed.appspot.com",
};


// Route paths
export const SIGN_IN_PATH = '/sign-in';
// export const TASKS_PATH = '/tasks';
export const TASKS_PATH = '/form';
export const POST_SIGN_IN_PATH = TASKS_PATH;
export const POST_SIGN_OUT_PATH = SIGN_IN_PATH;
