import { firebaseDb, firebaseAuth } from 'src/core/firebase';
import * as types from './action-types';

const database = firebaseDb;

export function retrieveCommentData(comments) {
  return {
    type: types.RETRIEVE_COMMENT_DATA,
    payload: comments,
  };
}

export function fetchComments() {
  return (dispatch, getState) => {
    const { auth } = getState();
    // console.log(getState());
    var user = firebaseAuth.currentUser;


    const comments = [];
    firebaseDb.ref('user-posts/'+ user.uid).orderByChild('created_at').once('value', snapshot => {
    // database.ref('comments').orderByChild('created_at').once('value', snapshot => {
      const data = snapshot.val();
      console.log(data);
      for (const key in data) {
        if ({}.hasOwnProperty.call(data, key)) {
          comments.push(data[key]);
        }
      }
      comments.sort((a, b) => {
        const createdA = a.created_at;
        const createdB = b.created_at;
        return createdB - createdA;
      });
      dispatch(retrieveCommentData(comments));
    });
  };
}

export function postComment(name, email, startDate, endDate, comment) {
  return (dispatch, getState) => {
    const { auth } = getState();
    // const newCommentKey = database.ref().child('comments/${auth.id}').push().key;
        // console.log(auth);
    var user = firebaseAuth.currentUser;

    firebaseAuth.onAuthStateChanged(function(user) {
    if (user) {
          const newCommentKey = firebaseDb.ref('posts').push().key;
    const created_at = +new Date();
    const updates = {};
    console.log(auth);
    var postData = {
      author: user.displayName,
      uid: user.uid,
      startDate: startDate,
      endDate: endDate,
      comment: comment,
      created_at: created_at,
      origin: 'PDX',
      destination: 'SFO',
      starCount: 0
    };
    updates['/user-posts/'+ user.uid + '/' + newCommentKey] = postData;
    updates['/posts/'+ newCommentKey] = postData;
    database.ref().update(updates);

    } 
  });

            // console.log(comments/${auth.id});
    // const newCommentKey = firebaseDb.ref(`comments/${auth.id}`).push().key;

    dispatch(fetchComments());
  };
}