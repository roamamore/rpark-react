import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { authReducer } from './auth';
import { notificationReducer } from './notification';
import { tasksReducer } from './tasks';
// import { formReducer } from './form';
import { reducer as formReducer } from 'redux-form';
import guestbookReducer from './form/reducer.js';


export default combineReducers({
  auth: authReducer,
  notification: notificationReducer,
  routing: routerReducer,
  tasks: tasksReducer,
  guestbook: guestbookReducer,
  form: formReducer,
});
