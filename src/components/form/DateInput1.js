let React = require('react');
const { PropTypes } = React;
// let DateTimePicker = require('react-widgets/lib/DateTimePicker');
let DateTimePicker = require('react-widgets').DateTimePicker;
const Moment = require('moment');
const momentLocalizer = require('react-widgets/lib/localizers/moment');
import 'react-widgets/dist/css/react-widgets.css';

// let FormField = require('./FormField');

momentLocalizer(Moment);

function DateInput1(props) {
	console.log(props)
	let { field , ...inputProps} = props;
	 let onChange = field.onChange;
	// onChange = function (value) {
	// 			field.value=value;
	// 		};
	return (
		<DateTimePicker
			// value={field.value}
			onChange={onChange}
			time={false}
		/>
	);
}

DateInput1.propTypes = {
	field: PropTypes.object.isRequired
};

module.exports = DateInput1;


// {...inputProps}
// 					format="dd/MM/yyyy"
// 					name={field.name}
// 					onChange={onChange}
// 					time={false}
// 					value={field.value}