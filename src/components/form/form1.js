import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { postComment } from '../../core/form/formActions';
// import DateInput1 from './DateInput1';

// let DateTimePicker = require('react-widgets').DateTimePicker;
// const Moment = require('moment');
// const momentLocalizer = require('react-widgets/lib/localizers/moment');
// import 'react-widgets/dist/css/react-widgets.css';

// momentLocalizer(Moment);

var DatePicker = require('react-datepicker');
var moment = require('moment');

require('react-datepicker/dist/react-datepicker.css');


class Form extends Component {
  static propTypes = {
    postComment: PropTypes.func,
    resetForm: PropTypes.func,
    handleSubmit: PropTypes.func,
    fields: PropTypes.object,
    // onChange: PropTypes.func,
  };

  onSubmit({ name, email, startDate, endDate, comment }) {
    console.log(startDate);
    this.props.postComment.call(this, name, email, startDate, endDate, comment);
    this.props.resetForm();
  }

      // onChange = function(value) {
      //   this.props.fields.startDate = value;
      // }
  //   handleChange = function(value) {
  //   this.props.startDate.value=value;
  // }

  render() {
    const { handleSubmit, fields: { name, email, startDate, endDate, comment } } = this.props;

let onstartChange = startDate.onChange;

      onChange = function (...args) {
        startDate.onChange(...args);
      };
// console.log(startDate);

    return (
      <form className="guestbook__form form-horizontal"
        onSubmit={handleSubmit(this.onSubmit.bind(this))} noValidate>
        <div className={`form-group ${name.touched && name.invalid ? 'has-error' : ''}`}>
          <label htmlFor="name" className="col-md-2 control-label">Name:</label>
          <div className="col-md-10">
            <input {...name} type="text" className="form-control" maxLength="20" />
            <div className="help-block">
              {name.touched ? name.error : ''}
            </div>
          </div>
        </div>

        <div className={`form-group ${email.touched && email.invalid ? 'has-error' : ''}`}>
          <label htmlFor="email" className="col-md-2 control-label">Email:</label>
          <div className="col-md-10">
            <input {...email} type="email" className="form-control" maxLength="100" />
            <div className="help-block">
              {email.touched ? email.error : ''}
            </div>
          </div>
        </div>


        <div className={`form-group ${startDate.touched && startDate.invalid ? 'has-error' : ''}`}>
          <label htmlFor="startDate" className="col-md-2 control-label">startDate:</label>
          <div className="col-md-10">

          <DatePicker
            {...startDate}
            dateFormat='MM/DD/YY'
            selected={startDate.value ? moment(startDate.value, "MM/DD/YY") : null }
            onChange={onChange}
            showYearDropdown 
            dateFormatCalendar="MMMM"
            minDate={moment()}
          />

            <div className="help-block">
              {startDate.touched ? startDate.error : ''}
            </div>
          </div>
        </div>

        <div className={`form-group ${endDate.touched && endDate.invalid ? 'has-error' : ''}`}>
          <label htmlFor="endDate" className="col-md-2 control-label">endDate:</label>
          <div className="col-md-10">

          <DatePicker
            {...endDate}
            dateFormat='MM/DD/YY'
            selected={endDate.value ? moment(endDate.value, "MM/DD/YY") : null }
            onChange={onChange}
            showYearDropdown 
            dateFormatCalendar="MMMM"
            minDate={moment()}
          />

            <div className="help-block">
              {endDate.touched ? endDate.error : ''}
            </div>
          </div>
        </div>
            


        <div className={`form-group ${comment.touched && comment.invalid ? 'has-error' : ''}`}>
          <label htmlFor="comment" className="col-md-2 control-label">Comment:</label>
          <div className="col-md-10">
            <textarea {...comment} className="form-control"></textarea>
            <div className="help-block">
              {comment.touched ? comment.error : ''}
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="col-md-offset-2 col-md-10">
            <button type="submit" className="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    );
  }
}

function isEmail(email){
  return /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test( email );
}

function validate(values) {
  const errors = {};

  if (!values.name) {
    errors.name = 'Please fill the name';
  }

  if (!values.email) {
    errors.email = 'Please fill the email';
  } else {
    if (!isEmail(values.email)) {
      errors.email = 'E-mail format is incorrect';
    }
  }
  if (!values.startDate) {
    errors.startDate = 'Please fill the start Date';
  }
  if (!values.endDate) {
    errors.endDate = 'Please fill the return Date';
  }

  if (!values.comment) {
    errors.comment = 'Please fill the comment';
  }

  return errors;
}


export default reduxForm({
  form: 'guestbook',
  fields: ['name', 'email', 'startDate' , 'endDate', 'comment'],
  validate,
}, null, { postComment })(Form);


            // <input {...startDate} type="text" className="form-control" maxLength="100" />

          //             <DatePicker
          //   {...startDate}
          //   dateFormat='MM/DD/YY'
          //   selected={startDate.value ? moment(startDate.value) : null }
          //   onChange={onChange}
          // />

                        // value={startDate.value}

            //                           <DateTimePicker
            //   time={false}
            //   onChange={onChange}
            //   value={startDate.value}
            // />