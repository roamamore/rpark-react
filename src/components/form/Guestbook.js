import React from 'react';
import Form from './form';
import List from './List';

const Guestbook = () => {
  return (
    <div className="guestbook">
      <Form />
      <hr />
      <List />
      
    </div>
  );
};

export default Guestbook;